package com.example.genericlivedata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val controlledLiveData by lazy { ControlledLiveData<String>() }

    private var counter = 0

    private val observerStack by lazy { Stack<androidx.lifecycle.Observer<String>>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        emit.setOnClickListener {
            Log.e("LiveData : ", "${++counter}")
            controlledLiveData.value = "$counter"
        }

        add.setOnClickListener {
            val stackCurrentSize = observerStack.size
            val observer = androidx.lifecycle.Observer<String> {
                Log.e("Observer : ${stackCurrentSize + 1} : ", it)
            }
            observerStack.add(observer)
            controlledLiveData.observe(this, observer)
        }

        add_single.setOnClickListener {
            val stackCurrentSize = observerStack.size
            val observer = androidx.lifecycle.Observer<String> {
                Log.e("Observer : ${stackCurrentSize + 1} : ", it)
            }
            controlledLiveData.observeOnce(this, observer)
        }

        remove.setOnClickListener {
            if (observerStack.isNotEmpty()) {
                val observer = observerStack.pop()
                controlledLiveData.removeObserverPermanently(observer)
            }
        }

        enable.setOnClickListener {
            controlledLiveData.enable()
        }

        disable.setOnClickListener {
            controlledLiveData.disable()
        }

    }

    /**
     * features tested
     * - once an observer is added , it emit current liveData value (only that new observer emits it not all)
     * - single observer emits once (on subscription only)
     * - disable allows the liveData to keep emitting but none of it's observers receive the event , because they're unsubscribed at this state
     *     - if an observer was added while LiveData is disabled is won't get initial emission
     *     - even single observer it'll get it's  first emission once LiveData is enabled then it'll be removed as it's a Single type
     * - enable updates all current observers with latest LiveData value and keeps updated them as LiveData emits
     * */
}