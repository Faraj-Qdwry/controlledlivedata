package com.example.genericlivedata

import androidx.lifecycle.*

/**
 * features tested
 * - once an observer is added , it emit current liveData value (only that new observer emits it not all)
 * - single observer emits once (on subscription only)
 * - disable allows the liveData to keep emitting but none of it's observers receive the event , because they're unsubscribed at this state
 *     - if an observer was added while LiveData is disabled is won't get initial emission
 *     - even single observer it'll get it's  first emission once LiveData is enabled then it'll be removed as it's a Single type
 * - enable updates all current observers with latest LiveData value and keeps updated them as LiveData emits
 * */

class ControlledLiveData<T> : MutableLiveData<T>() {

    private var observersMap = mutableMapOf<Observer<in T>, LifecycleOwner>() // keep track of all my observers
    private var isEnabled = true

    fun observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<in T>) {
        val singleObserver = object : Observer<T> {
            override fun onChanged(t: T?) {
                observer.onChanged(t)
                removeObserverPermanently(this)
            }
        }
        observe(lifecycleOwner, singleObserver)
    }

    /**
     * new observer should start observing only if isEnabled = true
     * */
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        observersMap[observer] = owner
        if (isEnabled) {
            super.observe(owner, observer)
        }
    }

    /**
     * users of this class shall use this insteadof removeObserver() cause that won't remove observer form @observersList
     */
    fun removeObserverPermanently(observer: Observer<in T>) {
        observersMap.remove(observer)
        removeObserver(observer)
    }

    /**
     * remove observers with LifecycleOwner
     * */
    fun removeObserversPermanently(owner: LifecycleOwner) {
        observersMap = observersMap.filter { it.value == owner }.toMutableMap()
        removeObservers(owner)
    }

    fun enable() {
        if (!isEnabled) {
            isEnabled = true
            observersMap.forEach { observe(it.value, it.key) }
        }
    }

    fun disable() {
        if (isEnabled) {
            observersMap.forEach { removeObserver(it.key) }
            isEnabled = false
        }
    }

}